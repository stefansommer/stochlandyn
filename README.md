# README #

This repository contains code for stochastic landmark dynamics as covered in the papers

Alexis Arnaudon, Darryl Holm, and Stefan Sommer: *A Geometric Framework for Stochastic Shape Analysis*


and

Alexis Arnaudon, Darryl Holm, Akshay Pai, and Stefan Sommer: *A Stochastic Large Deformation Model for Computational Anatomy*, Information Processing in Medical Imaging, 2017.

Please contact Stefan Sommer [sommer@di.ku.dk](Link URL) or Alexis Arnaudon.

### How do I get set up? ###

* Summary of set up
Install numpy, scipy, theano, jupyter, matplotlib, multiprocess:

```
#!python

    pip install numpy scipy theano jupyter matplotlib multiprocess
```
Use e.g. a Python 3 virtualenv:

```
#!python

    virtualenv -p python3 .
    source bin/activate
    pip install numpy scipy theano jupyter matplotlib multiprocess
```

Set theano options in ~/.theanorc, e.g.

```
#!python

    [global]
    floatX = float64
    device = cpu
    optimizer = fast_run
    cast_policy = numpy+floatX
```

Start jupyter notebook as in

```
#!bash

    export OMP_NUM_THREADS=1; jupyter notebook
```

### Configuration ###

General settings are specified in the file *src/params.py*.

### Notebooks ###
* Test notebooks
    - *test_deterministic.ipynb* :
     simple deterministic test, with shooting
        
    - *test_stochastic.ipynb* :
        test the stochastic landmark evolution, initial value problem and convergence
        
    - *test_moment_single.ipynb* :
        test the moment equation on a single landmark, no shooting (creates paper figure)
        
    - *test_moment_ellipse.ipynb* :
        test the moment equation on an ellispse, no shooting (creates paper figure)
        
    - *paper_moment_shooting_single_BFGS.ipynb* :
        test moment shooting with a single landmark and BFGS algorithm (creates paper figure)

    - *paper_moment_shooting_ellipse_BFGS.ipynb* :
        test moment shooting with ellipsis and BFGS algorithm (creates paper figure)
     
    - *paper_moment_shooting_single_DE.ipynb* :
        test moment shooting with a single landmark and differential evolution algorithm (creates paper figure)

    - *paper_moment_shooting_ellipse_DE.ipynb* :
        test moment shooting with ellipsis and differential evolution algorithm (creates paper figure)  
        
    - *analysis_single_paper.ipynb* :
        analysis of the moment shooting for single landmark (creates paper figure)
    
    - *analysis_ellipse_paper.ipynb* :
        analysis of the moment shooting for ellipse (creates paper figure)

    - *paper_mle_single.ipynb* :
        test the mle EM procedure on a single landmark, no shooting (creates paper figure)
        
    - *paper_mle_ellipse.ipynb* :
        test the MLE EM procedure on an ellispse, no shooting (creates paper figure)
        
    - *landmark_conf_ellipsis_paper.ipynb* :
        ellipse initial condition
        
    - *landmark_conf_N1_paper.ipynb* :
        single landmark initial condition
