#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import *

from src.landmark_sto import *
from src.landmark_det import *
from src.likelihoods import *

## guided diffusion for sampling bridges

print('Guidance scheme:', guidance_method)
print('max G steps:', max_guide_steps.eval())

if guidance_method == 'DelyonHu' and norm_sigma:
    print("sigma normalization is not needed for Delyon/Hu scheme")

if guidance_method == 'fast':
    attraction_force = theano.shared(1.)
    if sigma_kernel == 'qubic':
        if not norm_sigma:
            attraction_force.set_value((3./2.)**2)
    else:
        if not norm_sigma:
            print("using Gaussian kernels with unit attraction force")
            attraction_force.set_value((1.)**2)
    
    def P_no_lambda(q,p,dW):
        #return T.tensordot(p,T.tensordot(sigma_no_lambdaf(q),dW,[2,0]),[[0,1],[0,1]])
        return (sigma_no_lambdaf(q)*p[:,:,np.newaxis]*dW[np.newaxis,np.newaxis,:]).sum(2).sum(1).sum(0)
    
    dqP_no_lambda = lambda q,p,dW: T.grad(P_no_lambda(q,p,dW),p)
    dpP_no_lambda = lambda q,p,dW: -T.grad(P_no_lambda(q,p,dW),q)


# from where to guide
def G(t,x):
    guidsteps = theano.ifelse.ifelse(T.lt((Tend-t)/dt,1.*max_guide_steps),(Tend-t)/dt,1.*max_guide_steps)
    guidsteps = T.iround(guidsteps)
    (cout_end, updates_end) = theano.scan(fn=euler,
                                outputs_info=[x],
                                non_sequences=[theano.ifelse.ifelse(T.gt(guidsteps,T.constant(0)),(Tend-t)/guidsteps,T.constant(0.))],
                                n_steps=theano.ifelse.ifelse(T.gt(guidsteps,T.constant(0)),guidsteps,T.constant(1)))
    gx = cout_end[-1,0]

    return (gx, cout_end)
Gf = theano.function([t,x],G(t,x))

# guidance matrix
def Phi(x,t=None,dx=None):
    q = x[0]
    p = x[1]
    if guidance_method == 'DelyonHu':
        (sigma,sigmaplus) = sigmaplusf(q)
        Phi = sigmaplus
    elif guidance_method == 'fast':
        Phi = attraction_force*sigma_no_lambdaf(q).dimshuffle((2,0,1))
    elif guidance_method == 'differential':
        hh = T.zeros(J)
        Phi = theano.gradient.disconnected_grad(T.nlinalg.MatrixPinv()(
            T.jacobian(G(t,x+dx+T.stack((dqP(q,p,hh),dpP(q,p,hh))))[0].flatten(),hh)
            )).reshape((J,N,DIM))
    return Phi

def ode_guided(t,x):
    q = x[0]
    p = x[1]
    dx = ode_f(x)

    (Gx,_) = G(t,x)
    ytilde = Gx-qtarget0

    guidance = theano.ifelse.ifelse(T.lt(t,Tend-dt/2),
            -ytilde/(Tend-t),
            T.zeros_like(ytilde))

    h = T.tensordot(Phi(x,t,dx),guidance,((1,2),(0,1)))
    if guidance_method == 'DelyonHu':
        dq = dx[0]+guidance # guide q (guidance = dqP(q,p,h))
        dp = dx[1]+dpP(q,p,h)
    elif guidance_method == 'fast':
        dq = dx[0]+dqP_no_lambda(q,p,h) # note: check _no_lambda for fast scheme
        dp = dx[1]+dpP_no_lambda(q,p,h) # note: check _no_lambda for fast scheme
    elif guidance_method == 'differential':
        dq = dx[0]+dqP(q,p,h)
        dp = dx[1]+dpP(q,p,h)
    else:
        assert(False)
    
    return (T.stack((dq,dp)),h,ytilde,Gx)
ode_guidedf = theano.function([t,x],ode_guided(t,x),on_unused_input='ignore')

def int_logphi(t,x,ytilde,xtp1,dx,Gx,gx,dW):
    # Hessian term ignored for now
    q = x[0]
    tp1 = t+dt
    qtp1 = xtp1[0]
    (Gxtp1,_) = G(tp1,xtp1)
    ytildetp1 = Gxtp1-qtarget0

    Phix = Phi(x,t,dx)
    Phixtp1 = Phi(xtp1,tp1,dx)
    A    = T.tensordot(Phix,Phix,(0,0)).reshape((N,DIM,N,DIM))
    Atp1 = T.tensordot(Phixtp1,Phixtp1,(0,0)).reshape((N,DIM,N,DIM))

    dxbdxt = theano.gradient.Rop((Gx-x[0]).flatten(),x[0],dx[0]).reshape((N,DIM))
    #dxbdxt = T.tensordot(theano.gradient.jacobian((Gx-x[0]).flatten(),x).reshape((N,DIM,2,N,DIM))[:,:,0,:,:],dx[0],((1,2),(0,1))).reshape((N,DIM))
    t2 = theano.ifelse.ifelse(T.lt(t,Tend-dt/2),
            -T.tensordot(
                ytilde,
                T.tensordot(
                    A,
                    dxbdxt,
                    ((2,3),(0,1))
                    ),
                ((0,1),(0,1))
                )/(Tend-t),
            0.)
    t34 = theano.ifelse.ifelse(T.lt(tp1,Tend-dt/2),
            -T.tensordot(ytildetp1,T.tensordot(Atp1-A,ytildetp1,((2,3),(0,1))),((0,1),(0,1)))/(2*(Tend-tp1)),
            0.)
        
    if guidance_method == 'DelyonHu':
        t1 = 0.
    elif guidance_method in ['fast','differential']:
        t1 = theano.ifelse.ifelse(T.lt(t,Tend-dt/2),
                T.tensordot(ytilde,
                    T.tensordot(Phix,dW,(0,0))-T.tensordot(A,gx[0],((2,3),(0,1))),
                    ((0,1),(0,1))
                    )/(Tend-t),
                0.)
        
    return t1+t2+t34

def eulerheun_guided(dW,t,logphi,x,dWguided,loglike):
    q = x[0]

    mainstep = T.lt(t,Tend-dt*3/2) # special cases for last steps close to endpoint
    
    # first step
    (bti,hi,ytildei,Gxi) = ode_guided(t,x)
    gi = g(x,dW)
    dxi = bti*dt+gi
    xi = x+dxi

    # second step, ignored at last step before endpoint
    (bt,h,ytilde,Gx) = ode_guided(t+dt,xi)
    gx = g(xi,dW)

    # average, taking care of endpoint
    bt = theano.ifelse.ifelse(mainstep,(bt+bti)/2,bti)
    ytilde = theano.ifelse.ifelse(mainstep,(ytilde+ytildei)/2,ytildei)
    Gx = Gxi # theano.ifelse.ifelse(mainstep,(Gx+Gxi)/2,Gx) # ignoring averaging results in less complicated gradient in int_logphi
    gx = theano.ifelse.ifelse(mainstep,(gx+gi)/2,T.zeros_like(gx))

    # actual step
    dxt = bt*dt+gx
    xtp1 = x+dxt

    logphit = logphi+theano.gradient.disconnected_grad(int_logphi(t,x,ytilde,xtp1,dxt,Gx,gx,dW)) # gradient for logphi not needed for MLE

    if guidance_method in ['DelyonHu', 'differential']:
        dW = (1-.5*dt/(Tend-t))*dW+dt*hi # correction factor to account for correlation between dW and h in second Euler-Heun step
        if False:#basis == 'std':
            loglike = loglike+theano.ifelse.ifelse(mainstep, # do not include likelihood of last step
                    loglike_step_alt(lambdax(lambdas).norm(2,axis=1)*dW)[0],
                    0.)
        else:
            sigma = sigmaf(q)
            loglike = loglike+theano.ifelse.ifelse(mainstep,
                    loglike_step(q,T.tensordot(sigma,dW,(2,0)))[0],
                    0.)
    elif guidance_method == 'fast':
        if basis == 'std':
            dW = lambdax(lambdas).norm(2,axis=1)*(1-.5*dt/(Tend-t))*dW+dt*hi # as above, but hi already scaled by lambdas
            loglike = loglike+theano.ifelse.ifelse(mainstep,
                    loglike+loglike_step_alt(dW)[0],
                    0.)
        else:
            assert(False) # not implemented
    else:
        assert(False)
    return [t+dt,logphit,xtp1,dW,loglike]

# create loop symbolic loop
(rcout_guided, rupdates_guided) = theano.scan(fn=eulerheun_guided,
                                outputs_info=[T.constant(0.),
                                              T.constant(0.),
                                              x,
                                              T.zeros((J,)),
                                              T.constant(0.),
                                              ],
                                sequences=[dWs])

(rcout_guided_params, rupdates_guided_params) = theano.scan(fn=eulerheun_guided,
                                outputs_info=[T.constant(0.),
                                              T.constant(0.),
                                              params[0:2*N*DIM].reshape((2,N,DIM)),
                                              T.zeros((J,)),
                                              T.constant(0.),
                                              ],
                                sequences=[dWs])

rsim_guided_logalpha = rcout_guided[1][-1]
rsim_guided_loglike = rcout_guided[4][-1]
rsim_guided_xs_params = rcout_guided_params[2]
rsim_guided_logalpha_params = rcout_guided_params[1][-1]
rsim_guided_loglike_params = rcout_guided_params[4][-1]
drsim_guided_loglike_params = T.grad(rcout_guided_params[4][-1],params)

# compile it
rsimf_guided = function(inputs=[x,dWs], 
                            outputs=rcout_guided, 
                            updates=rupdates_guided,
                            mode='FAST_RUN')

rsimf_guided_loglike_params = function(inputs=[dWs], 
                            outputs=[rsim_guided_loglike_params,drsim_guided_loglike_params,rsim_guided_logalpha_params,rsim_guided_xs_params],
                            updates=rupdates_guided_params,
                            mode='FAST_RUN')

