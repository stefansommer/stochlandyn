#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import * 
 
################################################################################### 
# this file just contain the evolution of position and momentum, no shooting.     # 
################################################################################### 

def sigma_deriv(q):
    #returns the derivative of the gaussian kernel for the noise
    Dq= (q.dimshuffle(0,'x',1)-sigmax.dimshuffle('x',0,1)).dimshuffle(0,2,1)
    
    return -1/sigmaxi**2 * sigmaf(q)*Dq 
    
def moment_1(x):
    #fast q and p moment computation for gaussian noise only 
  
    q=x[0] 
    p=x[1] 
    
    gradHq= T.grad(H(q,p),q,disconnected_inputs='ignore')
    gradHp= T.grad(H(q,p),p,disconnected_inputs='ignore')
    q_next = gradHp + 0.5*(sigmaf(q)*sigma_deriv(q)).sum(2) #there may be a mistake here, to be checked!
    p_next = -gradHq + 0.5/sigmaxi**2 * (sigmaf(q)*sigmaf(q)*p[:,:,np.newaxis]).sum(2)
    
    return T.stack((q_next,p_next))

def euler_ito(x): 
    #compute a time step with the Euler scheme
    x_new = moment_1(x)
    
    q_next =  x[0]  + dt * x_new[0]
    p_next =  x[1]  + dt * x_new[1] 
    
    return T.stack((q_next,p_next)) 


# compile it 
(cout_ito, updates_ito) = theano.scan(fn=euler_ito, 
                                outputs_info=[x], 
                                n_steps=n_steps) 
 
simf_ito= function(inputs=[x],  
                            outputs=cout_ito,  
                            updates=updates_ito) 
 
 

#below are old slow and unused functions, but that are not specific to a choice of noise kernel
def H_sto(q,p): 
    #define the stochastic Hamiltonian 

    return T.tensordot(p,sigmaf(q),[[0,1],[0,1]])
 
def moment(q,p,order): 
    #compute the first moment, given p and q 

    if order is 0: 
        return q.flatten() 
    elif order is 1: 
        return p.flatten()  
    elif order is 2: 
        #this moment is $q_i^\alpha q_i^\beta$ 
        return (q[:,:,np.newaxis]*q[:,np.newaxis,:]).flatten() 

def ode_moment(x,order): 
    #compute the moment equation, using the Kolmogorov operator $LP= -\{h,P\} + \sum_i \frac12 \{\phi_i,\{\phi_i,P\}\}$ 

    q=x[0] 
    p=x[1] 
    if order is 0:
        shape_moment= N.eval()*DIM
    elif order is 1:
        shape_moment= N.eval()*DIM
    elif order is 2:
        shape_moment= N.eval()*DIM**2
        
    gradHq= T.grad(H(q,p),q,disconnected_inputs='ignore')
    gradHp= T.grad(H(q,p),p,disconnected_inputs='ignore')

    jacMq= T.jacobian(moment(q,p,order),q,disconnected_inputs='ignore')
    jacMp= T.jacobian(moment(q,p,order),p,disconnected_inputs='ignore')

    jacStoq = T.jacobian(H_sto(q,p),q,disconnected_inputs='ignore')
    jacStop = T.jacobian(H_sto(q,p),p,disconnected_inputs='ignore')

    #Hamiltonian part 
    Ham = T.tensordot(gradHq,jacMp,[[0,1],[1,2]]) - T.tensordot(gradHp,jacMq,[[0,1],[1,2] ]) 
    #double bracket part (inner bracket) 
    single = lambda q,p: (T.tensordot(jacStoq,jacMp,[[1,2],[1,2]]) - T.tensordot(jacStop,jacMq,[[1,2],[1,2]])).flatten()

    #double bracket part (outer bracket) 
    jacSq = T.jacobian(single(q,p),q,disconnected_inputs='ignore').reshape([J.eval(),shape_moment,N.eval(),DIM])
    jacSp = T.jacobian(single(q,p),p,disconnected_inputs='ignore').reshape([J.eval(),shape_moment,N.eval(),DIM])

    Double= T.tensordot(jacStoq,jacSp,[[0,1,2],[0,2,3]]) - T.tensordot(jacStop,jacSq,[[0,1,2],[0,2,3]]) 
    
    out = - Ham + 0.5 * Double
    
    return out 
"""
def euler_ito_old (x,dt):  
     
    q_next =  x[0]  + dt * ode_moment(x,0).reshape([N,DIM]) 
    p_next =  x[1]  + dt * ode_moment(x,1).reshape([N,DIM]) 
    
    return T.stack((q_next,p_next)) 
 
"""
