#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

import theano
import numpy as np
import theano.tensor as T

##########################################################################
# this file contains various object definitions, and standard parameters #
##########################################################################

########### landmark parameters ###############
DIM = 2 #dimension of the image
N = theano.shared(3) #number of landmarks

SIGMA = theano.shared(np.array(.5).astype(theano.config.floatX)) #radius of the landmark

#timestepping
Tend = 1
n_steps = theano.shared(1000)
dt = Tend/n_steps

########### noise parameters #################
basis = 'std' # 'std' # set this to std if using the standard basis for the noise and pairwise ordering of x and y vectors in J
J = theano.shared(24)  # number of sigma's
assert(J.eval()%DIM==0)

# define sigma fieldsspatial 
sigmax = theano.shared(np.zeros([J.eval(),DIM]).astype(theano.config.floatX))

#we now choose between different noise representations, and especially the number of parameters to represent them
reduced_parameters = 0
Gaussian_process_noise = 0 # specify noise correlation by the between point covariance of the displacements. Usually only makes sence with reduced parameters. J must equal N*DIM when Gaussian_process_noise is set
norm_sigma = 0 # normalize rows of sigma to always produce unit noise (disregarding lambdas)

########## bridge sampling ###################
guidance_method = 'DelyonHu'
#guidance_method = 'fast'
#guidance_method = 'differential' # slow but more precise
max_guide_steps = T.constant(8) # faster execution
#max_guide_steps = n_steps # default
#max_guide_steps = T.constant(0) # for visualizing Delyon/Hu scheme

if reduced_parameters is 1:
    #all the same values for the noise amplitude
    Js = T.constant(DIM) #number of parameters
    lambdas = theano.shared(np.zeros([Js.eval()]).astype(theano.config.floatX))
    lambdax = lambda pars: T.tile((pars[:,np.newaxis]*T.eye(DIM)).reshape((Js,DIM)),(J//DIM,1)).reshape((J,DIM))
else:
    #different values for sigmas
    Js = J*DIM #number of parameters
    lambdas = theano.shared(np.zeros([Js.eval()]).astype(theano.config.floatX))
    lambdax = lambda pars: pars.reshape((J,DIM,))

lambdasf = theano.function([],lambdas)
lambdaxf = theano.function([],lambdax(lambdas))

sigmaxi = theano.shared(.5) # width of noise kernel

# kernel for stochastic noise
sigma_kernel = 'K' # 'qubic'  or 'K' for geometric kernel


############# shooting parameters ######################
maxiter = 100 #maximum number of iteration for the shooting

############# parameters for MLE ######################
params = theano.shared(np.zeros([2*N.eval()*DIM+Js.eval()]).astype(theano.config.floatX)) # q0,p0,Js
# to avoid non-positive variance, we parameterize using exp/log
llambdas = T.vector()
lparams = T.vector()
def lambdasToParamsE(llambdas):
    return T.log(llambdas)
def paramsToLambdasE(lparams):
    return T.exp(lparams)
lambdasToParams = theano.function([llambdas],lambdasToParamsE(llambdas))
paramsToLambdas = theano.function([lparams],paramsToLambdasE(lparams))

#for moment shooting, coefficients for the loss function
coeffq= theano.shared(np.array(1.).astype(theano.config.floatX))
coeffqq= theano.shared(np.array(1.).astype(theano.config.floatX))

#target position and variance for moment matching and MLE
qtarget0 = theano.shared(np.zeros([N.eval(),DIM]).astype(theano.config.floatX))
qqtarget0 = theano.shared(np.zeros([N.eval(),DIM,N.eval(),DIM]).astype(theano.config.floatX))
