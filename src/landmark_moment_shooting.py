#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import *

from src.landmark_ito_shooting import *
from src.landmark_moment import *

#########################################
#compute the shooting method for moments#
#########################################

#compute the loss for the 2nd moment
#lossqq = T.sum(T.sqr(cout_moment[1][-1]-qqtarget0))
lossqq = (T.sqr(cout_moment[1][-1]-qqtarget0).sum(3).sum(1)/ T.sqr(qqtarget0).sum(3).sum(1)).sum(1).sum(0)/N.eval()**2 


#compile it
lossfqq = function(inputs=[x,qq,pq,pp], 
                            outputs=lossqq, 
                            updates=updates_moment)

#compute the total loss
lossX= coeffqq * lossqq #+ coeffq * lossq

#compile it
lossfX = function(inputs=[x,qq,pq,pp], 
                            outputs=lossX, 
                            updates=updates_moment)


#compute the gradients of the loss function
#dlossXx = T.grad(lossX,x)
dlossXsig = T.grad(lossX,lambdas)

#compile it
dlossfX = function(inputs=[x,qq,pq,pp], 
                            #outputs=[lossX, dlossXx, dlossXsig],
                            outputs=[lossX, dlossXsig], 
                            updates=updates_moment)



#the functions below should be used directly in the notebook, and modified case by case


#function for the shooting, with various methods
def shoot_moment_variance(q0,p0,qq0,save=True):
    state= []
    from scipy.optimize import basinhopping
    from scipy.optimize import differential_evolution
    
    def fopts(X):
        x = T.stack(q0,p0).astype(theano.config.floatX).eval()
        
        lambdas.set_value(X)

        [y,gysig] = dlossfX(x,qq0,0*qq0,0*qq0)
        
        if save:
            #output the system state in a pickle
            state.append([lambdas.eval(),p0.reshape([N.eval(),DIM]),y,y,y])
            out_file = open('shooting_moment.pkl', 'wb')
            pickle.dump(state, out_file)
            out_file.close()
            
        #print("Lambdas are {}".format(lambdas.eval()))
        print('Total loss: %.5e'%y)
    
        return (y,gysig.flatten())
    
    def func(X):
        
        x = T.stack(q0,p0).astype(theano.config.floatX).eval()
        
        lambdas.set_value(X)#[N.eval()*DIM:])

        y = lossfX(x,qq0,0*qq0,0*qq0)
        
        if save:
            #output the system state in a pickle
            #yqq= lossfqq(x,qq0,0*qq0,0*qq0)

            state.append([lambdas.eval(),p0.reshape([N.eval(),DIM]),y,y,y])
            out_file = open('shooting_moment.pkl', 'wb')
            pickle.dump(state, out_file)
            out_file.close()
        #print("Lambdas are {}".format(lambdas.eval()))
        print('Total loss: %.5e'%y)
    
        return y
    
    X0= lambdas.eval()
    
    Bp = np.array(len(p0.flatten())*[[None,None]])
    Bsig1= np.array(int(J.eval()/2)*[[0.,0.01],[0.,0.]])
    Bsig2= np.array(int(J.eval()/2)*[[0.,0.0],[0.,0.01]])
    Bsig= np.concatenate((Bsig1,Bsig2))

    B= np.concatenate((Bp,Bsig))
    def printout(xk,convergence):
        print('val%.5e'%convergence)
    #res = minimize(fopts, X0, method='L-BFGS-B', jac=True, options={'disp': False, 'maxiter': maxiter},bounds=Bsig)
    #res = minimize(fopts, X0, method='BFGS', jac=True, options={'disp': False, 'maxiter': maxiter})
    #res = minimize(fopts, X0, method='TNC', jac=True, options={'disp': False, 'maxiter': maxiter},bounds=Bsig)
    #res = minimize(fopts, X0, method='CG', jac=True, options={'disp': False, 'maxiter': maxiter})
    #res = basinhopping(fopts, X0, minimizer_kwargs={"method":"L-BFGS-B","jac":True,"bounds":Bsig}, disp=True)#,niter=20)#,T= 0.5)
    res = differential_evolution(func,bounds=Bsig,strategy= "best1bin",disp=True,popsize=2,maxiter=20,tol=0.1,callback=printout,mutation=0.2,recombination=0.9)

    return(res.x,res.fun)

#shoot with both centre and variance, does not work now
def shoot_moment_all(q0,p0,qq0,save=True):
    state= []

    def fopts(X):
        x = T.stack(q0,X[:N.eval()*DIM].reshape([N.eval(),DIM])).astype(theano.config.floatX).eval()
        
        lambdas.set_value(X[N.eval()*DIM:])

        [y,gyx,gysig] = dlossfX(x,qq0,0*qq0,0*qq0)
        
        if save:
            #output the system state in a pickle
            yqq= lossfqq(x,qq0,0*qq0,0*qq0)
            yq= lossfq(x)

            state.append([lambdas.eval(),X[:N.eval()*DIM].reshape([N.eval(),DIM]),y,yq,yqq])
            out_file = open('shooting_moment.pkl', 'wb')
            pickle.dump(state, out_file)
            out_file.close()
        print("Lambdas are {}".format(lambdas.eval()))
     
        print('Total loss: %.5e'%y + '  position loss: %.5e'%yq + '  variance loss: %.5e'%yqq )
        
        return (y,T.concatenate((gyx[1].flatten(),gysig.flatten())).eval())

    X0= T.concatenate(( p0.flatten(), lambdas.eval().flatten())).astype(theano.config.floatX).eval()
    
    Bp = np.array(len(p0.flatten())*[[None,None]])
    Bsig1= np.array(int(J.eval()/2)*[[0,0.1],[0,0]])
    Bsig2= np.array(int(J.eval()/2)*[[0,0.0],[0,0.1]])
    Bsig= np.concatenate((Bsig1,Bsig2))

    B= np.concatenate((Bp,Bsig))

    #res = minimize(fopts, X0, method='L-BFGS-B', jac=True, options={'disp': False, 'maxiter': maxiter},bounds=B)
    #res = minimize(fopts, X0, method='BFGS', jac=True, options={'disp': False, 'maxiter': maxiter})
    #res = minimize(fopts, X0, method='TNC', jac=True, options={'disp': False, 'maxiter': maxiter},bounds=B)
    res = minimize(fopts, X0, method='CG', jac=True, options={'disp': False, 'maxiter': maxiter})

    return(res.x,res.fun)
