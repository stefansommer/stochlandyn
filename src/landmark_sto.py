#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import *

############################################################
######### stochastic ODE integration #######################
############################################################

#define the noise realisations
srng = RandomStreams()#seed=42)
dWs = srng.normal((n_steps,J), std=np.sqrt(dt))
dWsf = function([],dWs)

# stochastic addition to the Hamiltonian
def P(q,p,dW):
    #return T.tensordot(p,T.tensordot(sigmaf(q),dW,[2,0]),[[0,1],[0,1]])
    return (sigmaf(q)*p[:,:,np.newaxis]*dW[np.newaxis,np.newaxis,:]).sum(2).sum(1).sum(0)


dqP = lambda q,p,dW: T.grad(P(q,p,dW),p)
dpP = lambda q,p,dW: -T.grad(P(q,p,dW),q)

def g(x,dW):    
    if sigma_kernel == 'K' and not norm_sigma:
        dqPt = (sigmaf(x[0])*dW[np.newaxis,np.newaxis,:]).sum(2) 
        dpPt = 1./sigmaxi**2*((sigmaf(x[0])*x[1][:,:,np.newaxis]).sum(1)[:,np.newaxis,:]*(x[0][:,:,np.newaxis]-sigmax.dimshuffle(1,0)[np.newaxis,:,:])*dW[np.newaxis,np.newaxis,:]).sum(2)
    else:
        dqPt = dqP(x[0],x[1],dW)
        dpPt = dpP(x[0],x[1],dW)

    return T.stack((dqPt,dpPt))

dW = T.vector()
gf = function([x,dW],g(x,dW))

def ode_f(x):
    dqt = dq(x[0],x[1])
    dpt = dp(x[0],x[1])
        
    return T.stack((dqt,dpt))

def eulerheun(dW,x):
    gi = g(x,dW)
    txi = x + gi
    return x + dt*ode_f(x) + 0.5*(gi + g(txi,dW))

eulerheunf = function([dW,x],eulerheun(dW,x))

# create loop symbolic loop
(rcout, rupdates) = theano.scan(fn=eulerheun,
                                outputs_info=[x],                                
                                sequences=[dWs])

# compile it
rsimf = function(inputs=[x,dWs], 
                            outputs=rcout, 
                            updates=rupdates)


"""
#shooting method with random paths
# loss functions
rloss = 1./N*T.sum(T.sqr(rcout[-1,0]-qtarget0))
drloss = T.grad(rloss,x)

rlossf = function(inputs=[x,dt,dWs], 
                            outputs=rloss, 
                            updates=rupdates)
drlossf = function(inputs=[x,dt,dWs], 
                            outputs=[rloss, drloss], 
                            updates=rupdates)


# do the random shooting
def rshoot(q0,p0,dWs0):
    def fopts(x):
        [y,gy] = drlossf(np.array([q0,x.reshape([N.eval(),DIM])]).astype(theano.config.floatX),dWs0)
        return (y,gy[1].flatten())
 
    res = minimize(fopts, p0.flatten(), method='L-BFGS-B', jac=True, options={'disp': False, 'maxiter': maxiter})
    
    return(res.x,res.fun,dWs0)
"""
