#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import *
from src.landmark_ito import * 
 
########################################################## 
# this file contains the evolution of the second moment. # 
########################################################## 


def moment_qq(x,Dqq,Dpq, Dpp):
    
    #some shortcuts
    q=x[0] 
    p=x[1] 
    
    Dpq_diag1= Dpq.dimshuffle(1,3,0,2).diagonal(axis1=2,axis2=3).dimshuffle(2,0,1) #N,DIM,DIM 
    Dpp_diag1= Dpp.dimshuffle(1,3,0,2).diagonal(axis1=2,axis2=3).dimshuffle(2,0,1) #N,DIM,DIM 

    #######
    ## Q ##
    #######
    
    q_new = (p.dimshuffle(1,0)[np.newaxis,:,:] * Ker(q,q)[:,np.newaxis,:]).sum(2)
    
    q_new -= 0.5/sigmaxi**2 * (sigmaf(q)[:,:,np.newaxis,:] * sigmaf(q)[:,np.newaxis,:,:]*(q[:,np.newaxis,:,np.newaxis]-sigmax.dimshuffle(1,0)[np.newaxis,np.newaxis,:,:])).sum(3).sum(2)
    
    #######
    ## P ##
    #######
    
    tmp = q[:,:,np.newaxis,np.newaxis]*p[:,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,:,:]
    tmp += Dpp.dimshuffle(0,2,1,3).diagonal(axis1=2,axis2=3)[:,np.newaxis,:,:] * q[:,:,np.newaxis,np.newaxis]
    tmp += Dpq.dimshuffle(2,3,0,1)*p[:,np.newaxis,np.newaxis,:]
    tmp += Dpq_diag1.dimshuffle(0,2,1)[:,:,np.newaxis,:] * p[np.newaxis,np.newaxis,:,:]
    
    tmp -= q.dimshuffle(1,0)[np.newaxis,:,:,np.newaxis]*p[:,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,:,:]
    tmp -= Dpp.dimshuffle(0,2,1,3).diagonal(axis1=2,axis2=3)[:,np.newaxis,:,:] * q.dimshuffle(1,0)[np.newaxis,:,:,np.newaxis]
    tmp -= Dpq_diag1.dimshuffle(2,0,1)[np.newaxis,:,:,:]*p[:,np.newaxis,np.newaxis,:]
    tmp -= Dpq.dimshuffle(0,3,2,1) * p[np.newaxis,np.newaxis,:,:]
    
    p_new = 1./SIGMA**2 * (Ker(q,q)[:,np.newaxis,:,np.newaxis]*tmp).sum(3).sum(2)
    
    p_new += 0.5/sigmaxi**2 * ((p[:,np.newaxis,:,np.newaxis] * sigmaf(q)[:,np.newaxis,:,:]).sum(2)*sigmaf(q)).sum(2)
    
    ########
    ## QQ ##
    ########
    
    #A_{qq}
    Dqq_new = (Ker(q,q)[np.newaxis,np.newaxis,:,np.newaxis,:]*Dpq.dimshuffle(2,3,1,0)[:,:,np.newaxis,:,:]).sum(4)
    Dqq_new += (Ker(q,q)[:,np.newaxis,np.newaxis,np.newaxis,:]*Dpq.dimshuffle(1,2,3,0)[np.newaxis,:,:,:,:]).sum(4)
    
    #B_{qq}
    Dqq_new += (sigmaf(q)[:,:,np.newaxis,np.newaxis,:]*sigmaf(q)[np.newaxis,np.newaxis,:,:,:]).sum(4) #N,DIM,N,DIM, sum(J)
    
    #C_{qq}
    Dqq_new -= 0.5/sigmaxi**2 * (Dqq[:,:,:,np.newaxis,:]*(sigmaf(q)[np.newaxis,np.newaxis,:,:,np.newaxis,:]*sigmaf(q)[np.newaxis,np.newaxis,:,np.newaxis,:,:]).sum(5)).sum(4)
    Dqq_new -= 0.5/sigmaxi**2 * (Dqq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,:]*(sigmaf(q)[:,:,np.newaxis,np.newaxis,np.newaxis,:]*sigmaf(q)[:,np.newaxis,np.newaxis,np.newaxis,:,:]).sum(5)).sum(4)
    
    
    ########
    ## PP ##
    ########
    
    #A_{pp}
    
    tmp = Dpp[:,:,:,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]*q[np.newaxis,np.newaxis,:,:,np.newaxis,np.newaxis]
    tmp += Dpp[:,:,np.newaxis,np.newaxis,:,:]*p[np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]*q[np.newaxis,np.newaxis,:,:,np.newaxis,np.newaxis]
    tmp += Dpq[:,:,:,:,np.newaxis,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]
    tmp += Dpp[:,:,:,np.newaxis,np.newaxis,:]*Dpq.dimshuffle(2,3,0,1)[np.newaxis,np.newaxis,:,:,:,:]
    
    tmp += Dpp[:,:,np.newaxis,np.newaxis,:,:]*Dpq_diag1.dimshuffle(0,2,1)[np.newaxis,np.newaxis,:,:,np.newaxis,:]
    tmp += Dpq[:,:,:,:,np.newaxis,np.newaxis]*Dpp.dimshuffle(0,2,1,3).diagonal(axis1=2,axis2=3)[np.newaxis,np.newaxis,:,np.newaxis,:,:]
    

    tmp -= Dpp[:,:,:,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]*q.dimshuffle(1,0)[np.newaxis,np.newaxis,np.newaxis,:,:,np.newaxis]
    tmp -= Dpp[:,:,np.newaxis,np.newaxis,:,:]*p[np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]*q.dimshuffle(1,0)[np.newaxis,np.newaxis,np.newaxis,:,:,np.newaxis]
    tmp -= Dpq.dimshuffle(0,1,3,2)[:,:,np.newaxis,:,:,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]
    tmp -= Dpp[:,:,:,np.newaxis,np.newaxis,:]*Dpq_diag1.dimshuffle(2,0,1)[np.newaxis,np.newaxis,np.newaxis,:,:,:]
    tmp -= Dpp[:,:,np.newaxis,np.newaxis,:,:]*Dpq.dimshuffle(0,3,2,1)[np.newaxis,np.newaxis,:,:,:,:]
    tmp -= Dpq.dimshuffle(0,1,3,2)[:,:,np.newaxis,:,:,np.newaxis]*Dpp.dimshuffle(0,2,1,3).diagonal(axis1=2,axis2=3)[np.newaxis,np.newaxis,:,np.newaxis,:,:]
    
    Dpp_new = 1./SIGMA**2 * (Ker(q,q)[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis]*tmp).sum(5).sum(4) #N,DIM,N,DIM,sum(N),sum(DIM) 
    
    tmpT = Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis]
    tmpT += Dpp[np.newaxis,np.newaxis,:,:,:,:]*p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis]
    tmpT += Dpq.dimshuffle(2,3,0,1)[:,:,:,:,np.newaxis,np.newaxis]*p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]
    tmpT += Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:]*Dpq.dimshuffle(2,3,0,1)[:,:,np.newaxis,np.newaxis,:,:]
    tmpT += Dpp[np.newaxis,np.newaxis,:,:,:,:]*Dpq_diag1.dimshuffle(0,2,1)[:,:,np.newaxis,np.newaxis,np.newaxis,:]
    tmpT += Dpq.dimshuffle(2,3,0,1)[:,:,:,:,np.newaxis,np.newaxis]*Dpp.dimshuffle(0,2,1,3).diagonal(axis1=2,axis2=3)[:,np.newaxis,np.newaxis,np.newaxis,:,:]
    
    tmpT -= Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]*q.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,:,np.newaxis]
    tmpT -= Dpp[np.newaxis,np.newaxis,:,:,:,:]*p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*q.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,:,np.newaxis]
    tmpT -= Dpq.dimshuffle(3,0,1,2)[np.newaxis,:,:,:,:,np.newaxis]*p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]
    tmpT -= Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:]*Dpq_diag1.dimshuffle(2,0,1)[np.newaxis,:,np.newaxis,np.newaxis,:,:]
    tmpT -= Dpp[np.newaxis,np.newaxis,:,:,:,:]*Dpq.dimshuffle(0,3,2,1)[:,:,np.newaxis,np.newaxis,:,:]
    tmpT -= Dpq.dimshuffle(3,0,1,2)[np.newaxis,:,:,:,:,np.newaxis]*Dpp.dimshuffle(0,2,1,3).diagonal(axis1=2,axis2=3)[:,np.newaxis,np.newaxis,np.newaxis,:,:]
    
    Dpp_new += 1./SIGMA**2 * (Ker(q,q)[:,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*tmpT).sum(5).sum(4) #N,DIM,N,DIM,sum(N),sum(DIM)
    
    
    #B_{pp}
    tmp = p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,np.newaxis]*q[np.newaxis,np.newaxis,:,:,np.newaxis,np.newaxis,np.newaxis]
    tmp += Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,np.newaxis,:,:,np.newaxis]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,np.newaxis]*q[np.newaxis,np.newaxis,:,:,np.newaxis,np.newaxis,np.newaxis]
    tmp += Dpq.dimshuffle(2,3,0,1)[:,:,:,np.newaxis,:,np.newaxis,np.newaxis]*p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*q[np.newaxis,np.newaxis,:,:,np.newaxis,np.newaxis,np.newaxis]
    tmp += Dqq[:,:,:,:,np.newaxis,np.newaxis,np.newaxis]*p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]
    tmp += Dpq_diag1.dimshuffle(0,2,1)[:,:,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]*q[np.newaxis,np.newaxis,:,:,np.newaxis,np.newaxis,np.newaxis]
    tmp += Dpq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,np.newaxis]
    tmp += Dpq_diag1.dimshuffle(0,2,1)[np.newaxis,np.newaxis,:,:,:,np.newaxis,np.newaxis]*p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,np.newaxis]
    
    tmp += Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,np.newaxis,:,:,np.newaxis]*Dqq[:,:,:,:,np.newaxis,np.newaxis,np.newaxis]
    tmp += Dpq_diag1.dimshuffle(0,2,1)[:,:,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*Dpq_diag1.dimshuffle(0,2,1)[np.newaxis,np.newaxis,:,:,:,np.newaxis,np.newaxis]
    tmp += Dpq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:,np.newaxis]*Dpq.dimshuffle(2,3,0,1)[:,:,:,np.newaxis,:,np.newaxis,np.newaxis]
    
    tmp -= p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,np.newaxis]*sigmax.dimshuffle(1,0)[np.newaxis,np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]
    tmp -= q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,np.newaxis]*Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,np.newaxis,:,:,np.newaxis]*sigmax.dimshuffle(1,0)[np.newaxis,np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]
    tmp -= p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*Dpq.dimshuffle(2,3,0,1)[:,:,:,np.newaxis,:,np.newaxis,np.newaxis]*sigmax.dimshuffle(1,0)[np.newaxis,np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]
    tmp -= p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]*Dpq_diag1.dimshuffle(0,2,1)[:,:,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*sigmax.dimshuffle(1,0)[np.newaxis,np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]
 
    tmp -= p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]*q[np.newaxis,np.newaxis,:,:,np.newaxis,np.newaxis,np.newaxis]*sigmax.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]
    tmp -= q[np.newaxis,np.newaxis,:,:,np.newaxis,np.newaxis,np.newaxis]*Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,np.newaxis,:,:,np.newaxis]*sigmax.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]
    tmp -= p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*Dpq_diag1.dimshuffle(0,2,1)[np.newaxis,np.newaxis,:,:,:,np.newaxis,np.newaxis]*sigmax.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]
    tmp -= p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]*Dpq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:,np.newaxis]*sigmax.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]
    
    tmp += (p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*p[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,np.newaxis]+ Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,np.newaxis,:,:,np.newaxis])*sigmax.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*sigmax.dimshuffle(1,0)[np.newaxis,np.newaxis,np.newaxis,:,np.newaxis,np.newaxis,:]
    
    Dpp_new += 1./(sigmaxi**4) * (sigmaf(q)[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]*sigmaf(q)[np.newaxis,np.newaxis,:,np.newaxis,:,np.newaxis,:]*tmp).sum(6).sum(5).sum(4)
  
    #C_{pp}
    Dpp_new += .5/ (sigmaxi**2) * (Dpp[:,:,:,np.newaxis,:] * ( sigmaf(q)[np.newaxis,np.newaxis,:,:,np.newaxis,:]* sigmaf(q)[np.newaxis,np.newaxis,:,np.newaxis,:,:]).sum(5)).sum(4)
    Dpp_new += .5/ (sigmaxi**2) * (Dpp.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,:] * ( sigmaf(q)[:,:,np.newaxis,np.newaxis,np.newaxis,:]* sigmaf(q)[:,np.newaxis,np.newaxis,np.newaxis,:,:]).sum(5)).sum(4)
    
    
    ########
    ## PQ ##
    ########
    
    # A_{pq}
    Dpq_new = (Ker(q,q)[np.newaxis,np.newaxis,:,np.newaxis,:]*Dpp.dimshuffle(0,1,3,2)[:,:,np.newaxis,:,:]).sum(4)
    
    tmp = Dpq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:]* p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis]
    tmp += Dpq.dimshuffle(2,3,0,1)[np.newaxis,np.newaxis,:,:,:,:]* p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis]
    tmp += Dqq[:,:,:,:,np.newaxis,np.newaxis]* p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]
    tmp += Dpq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:]* Dpq.dimshuffle(2,3,0,1)[:,:,np.newaxis,np.newaxis,:,:]
    tmp += Dpq.dimshuffle(2,3,0,1)[np.newaxis,np.newaxis,:,:,:,:]* Dpq_diag1.dimshuffle(0,2,1)[:,:,np.newaxis,np.newaxis,np.newaxis,:]
    tmp += Dqq[:,:,:,:,np.newaxis,np.newaxis]* Dpp.dimshuffle(0,2,1,3).diagonal(axis1=2,axis2=3)[:,np.newaxis,np.newaxis,np.newaxis,:,:]
    
    tmp -= Dpq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:]* p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]*q.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,:,np.newaxis]
    tmp -= Dpq.dimshuffle(2,3,0,1)[np.newaxis,np.newaxis,:,:,:,:]* p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*q.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,:,np.newaxis]
    tmp -= Dqq.dimshuffle(1,2,3,0)[np.newaxis,:,:,:,:,np.newaxis]* p[:,np.newaxis,np.newaxis,np.newaxis,np.newaxis,:]*p[np.newaxis,np.newaxis,np.newaxis,np.newaxis,:,:]
    tmp -= Dpq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,np.newaxis,:]* Dpq_diag1.dimshuffle(2,0,1)[np.newaxis,:,np.newaxis,np.newaxis,:,:]
    tmp -= Dpq.dimshuffle(2,3,0,1)[np.newaxis,np.newaxis,:,:,:,:]* Dpq.dimshuffle(0,3,2,1)[:,:,np.newaxis,np.newaxis,:,:]
    tmp -= Dqq.dimshuffle(1,2,3,0)[np.newaxis,:,:,:,:,np.newaxis]* Dpp.dimshuffle(0,2,1,3).diagonal(axis1=2,axis2=3)[:,np.newaxis,np.newaxis,np.newaxis,:,:]
    
    Dpq_new += 1./SIGMA**2 * (Ker(q,q)[:,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis] * tmp).sum(5).sum(4)
    
    #B_{pq}
    Dpq_new += 1./(sigmaxi**2) * ( (p[:,np.newaxis,np.newaxis,np.newaxis,:,np.newaxis]*(q[:,:,np.newaxis,np.newaxis,np.newaxis,np.newaxis]-sigmax.dimshuffle(1,0)[np.newaxis,:,np.newaxis,np.newaxis,np.newaxis,:]) + Dpq_diag1.dimshuffle(0,2,1)[:,:,np.newaxis,np.newaxis,:,np.newaxis] ) * sigmaf(q)[np.newaxis,np.newaxis,:,:,np.newaxis,:]* sigmaf(q)[:,np.newaxis,np.newaxis,np.newaxis,:,:]).sum(5).sum(4) 
    
    #C_{pq}
    Dpq_new -= 0.5/ (sigmaxi**2) * (Dpq[:,:,:,np.newaxis,:,np.newaxis]*sigmaf(q)[np.newaxis,np.newaxis,:,np.newaxis,:,:]*sigmaf(q)[np.newaxis,np.newaxis,:,:,np.newaxis,:]).sum(5).sum(4)
    
    Dpq_new += 0.5/(sigmaxi**2) * ((Dpq.dimshuffle(0,2,3,1)[:,np.newaxis,:,:,:,np.newaxis] * sigmaf(q)[:,:,np.newaxis,np.newaxis,np.newaxis,:])*sigmaf(q)[:,np.newaxis,np.newaxis,np.newaxis,:,:] ).sum(5).sum(4)
    
    return q_new, p_new, Dqq_new, Dpq_new, Dpp_new

#euler scheme for the moments
def euler_moment(x,qq,pq,pp): 
    
    q_tmp, p_tmp, qq_tmp, pq_tmp, pp_tmp = moment_qq(x,qq,pq,pp)
    
    q_new =  x[0] + dt * q_tmp
    p_new =  x[1] + dt * p_tmp 
    x_new = T.stack((q_new,p_new))
    
    qq_new =  qq + dt * qq_tmp
    pq_new =  pq + dt * pq_tmp 
    pp_new =  pp + dt * pp_tmp 

    return x_new, qq_new, pq_new, pp_new

qq = T.tensor4('qq') 
pq = T.tensor4('pq') 
pp = T.tensor4('pp') 

 
#do the loop 
(cout_moment, updates_moment) = theano.scan(fn=euler_moment, 
                                outputs_info=[x,qq,pq,pp], 
                                n_steps=n_steps) 
 
# compile it 
simf_moment = function(inputs=[x,qq,pq,pp],  
                            outputs=cout_moment,  
                            updates=updates_moment)
