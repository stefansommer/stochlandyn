#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

#setup file for commonly used python libraries, add here any specific paths for your installation configuration


cx1= False
if cx1 is True:
    import sys 

    print( '\n'.join(sys.path))
    sys.path.remove('/apps/python/3.4.2/lib/python3.4/site-packages/numpy-1.9.2-py3.4-linux-x86_64.egg')
    sys.path.remove('/apps/python/3.4.2/lib/python3.4/site-packages/scipy-0.15.1-py3.4-linux-x86_64.egg')


    sys.path.append("/home/aa10213/.local/lib/python3.4/site-packages/scipy-0.18.0-py3.4.egg")
    sys.path.append("/home/aa10213/.local/lib/python3.4/site-packages/numpy-1.11.1-py3.4.egg")

    import scipy
    scipy.version.version
    import numpy
    numpy.version.version
    
import theano 
from theano import pp, function 
import theano.tensor as T 
from theano.tensor.shared_randomstreams import RandomStreams 
 
import numpy as np 
import time

from src.params import *
from src.utils import *
from src.multiprocess_utils import *
from src.plotting import *

