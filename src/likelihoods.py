#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import *
import src.nlinalg as nlinalg

def loglike_step(q,sigmadW):
    sigma = sigmalambdaf(q.reshape((N,DIM)),paramsToLambdasE(params[2*N*DIM:2*N*DIM+Js])).reshape((N*DIM,J))
    Cov = dt*T.tensordot(sigma,sigma,(1,1))
    Pres =  T.nlinalg.MatrixInverse()(Cov) # precision matrix (inverse covariance)
    sigmadW = sigmadW.reshape((N*DIM,))
    residual = T.tensordot(sigmadW,T.tensordot(Pres,sigmadW,axes=(1,0)),axes=(0,0))
    loglike = .5*(-N*DIM*T.log(2*np.pi)+nlinalg.LogAbsDet()(Pres)-residual)
    return (loglike,residual)

def loglike_step_alt(dW):
    Cov = dt*T.sqr(lambdax(paramsToLambdasE(params[2*N*DIM:2*N*DIM+Js])).norm(2,axis=1))
    Pres = 1./Cov
    residual = T.dot(dW.flatten(),Pres*dW.flatten())
    loglike = .5*(-J*T.log(2*np.pi)+T.log(Pres).sum()-residual)
    return (loglike,residual)
