#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import *

from src.landmark_ito_shooting import *
from src.landmark_moment import *

#########################################
#compute the shooting method for moments#
#########################################

#compute the loss for the 2nd moment
#lossqq = T.sum(T.sqr(cout_moment[1][-1]-qqtarget0))
lossqq = (T.sqr(cout_moment[1][-1]-qqtarget0).sum(3).sum(1)/ T.sqr(qqtarget0).sum(3).sum(1)).sum(1).sum(0)/N.eval()**2 

#compile it
lossfqq = function(inputs=[x,qq,pq,pp], 
                            outputs=lossqq, 
                            updates=updates_moment)

#compute the total loss
lossX= coeffqq * lossqq #+ coeffq * lossq

#compile it
lossfX = function(inputs=[x,qq,pq,pp], 
                            outputs=lossX, 
                            updates=updates_moment)
