#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

import theano
from theano import pp, function
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

import numpy as np
import time

from src.params import *

###############################################
# File that contains various useful functions #
###############################################

x = T.tensor3('x')
t = T.scalar()

#general kernel for any sigma
def Ksigma(q1,q2,sigma):
    r_sq = T.sqr(q1.dimshuffle(0,'x',1)-q2.dimshuffle('x',0,1)).sum(2)
    
    return T.exp( - r_sq / (2.*sigma**2) )

# kernel function for the landmarks
def Ker(q1,q2):
    return Ksigma(q1,q2,SIGMA)

#Hamiltonian
def H(q,p):
    return 0.5*T.tensordot(p,T.tensordot(Ker(q,q),p,[[1],[0]]),[[0,1],[0,1]])

# splitted version of the kernel (lower memory requirements)
def Ksplit(q1,q2):
    if (DIM == 2):
        r_sq = T.sqr(q1[:,0].dimshuffle(0,'x')-q2[:,0].dimshuffle('x',0)) \
            + T.sqr(q1[:,1].dimshuffle(0,'x')-q2[:,1].dimshuffle('x',0))
    else:
        r_sq = T.sqr(q1[:,0].dimshuffle(0,'x')-q2[:,0].dimshuffle('x',0)) \
            + T.sqr(q1[:,1].dimshuffle(0,'x')-q2[:,1].dimshuffle('x',0)) \
            + T.sqr(q1[:,2].dimshuffle(0,'x')-q2[:,2].dimshuffle('x',0))
    return T.exp( - r_sq / (2*SIGMA**2) )

splits = theano.shared(100)


#splitted version of the Hamiltonian
def Hsplits(q1,p1,q2,p2):
    return 0.5*T.tensordot(p1,T.tensordot(Ksplit(q1,q2),p2,[[1],[0]]),[[0,1],[0,1]])

def Hsplit(q,p):
    q1s = q.reshape([splits,q.shape[0]//splits,q.shape[1]])
    p1s = p.reshape([splits,p.shape[0]//splits,p.shape[1]])
    
    (Hsplitsout, updates) = theano.scan(fn=Hsplits,
                                non_sequences=[q,p],
                                sequences=[q1s,p1s])
    
    return Hsplitsout.sum()

#to use the split Hamiltonian uncomment
#H = Hsplit

#compile the previous functions
xe = T.tensor3('x')
pe = T.matrix('p')
qe = T.matrix('q')
qe1 = T.matrix('q')
qe2 = T.matrix('q')
xipe = T.matrix('p')
xiqe = T.matrix('q')
sigma = T.scalar()
Kf = function([qe1,qe2], Ker(qe1,qe2))
Ksigmaf = function([qe1,qe2,sigma], Ksigma(qe1,qe2,sigma))
Hf = function([qe,pe],H(qe,pe))

# compute gradients
dq = lambda q,p: T.grad(H(q,p),p)
dp = lambda q,p: -T.grad(H(q,p),q)
dqf = function([qe,pe], dq(qe,pe))
dpf = function([qe,pe], dp(qe,pe))

def setSplits(N):
    if (N <= 1000):
        splits.set_value(1)
    elif (N <= 200000):
        splits.set_value(N/1000) # 1000 particle interactions computed per split
    else:
        splits.set_value(N/500) # 500 particle interactions computed per split    
    assert(N % splits.get_value() == 0)


# B-spline kernel
B0 = lambda t: (-t**3+3*t**2-3*t+1)/6
B1 = lambda t: (3*t**3-6*t**2+4)/6
B2 = lambda t: (-3*t**3+3*t**2+3*t+1)/6
B3 = lambda t: t**3/6
c = lambda t,m: T.le(0,t+m-1) & T.le(t+m-1,1)
p3 = lambda t: c(t,0)*B0(t-T.floor(t))+c(t,1)*B1(t-T.floor(t))+c(t,2)*B2(t-T.floor(t))+c(t,3)*B3(t-T.floor(t))
ts = T.vector()
p3f = theano.function([ts],p3(ts))

def Kp3(q1,q2,sigma):
    r_sq = T.sqr(q1.dimshuffle(0,'x',1)-q2.dimshuffle('x',0,1)).sum(2)
    r = T.sqrt(r_sq+1e-8*(r_sq<1e-8))
    return p3(r/sigma)

# gaussian kernel
def KerK(q1,q2,sigma):
    return Ksigma(q1,q2,sigma)

# choice of kernel for sigma 
if sigma_kernel == 'qubic':
    Kstoch = Kp3 # qubic b-spline
else:
    Kstoch = KerK
    
#compile the kernel function
Kstochf = theano.function([qe],Kstoch(qe,sigmax,sigmaxi))

def Ksto_sigma(q):
    Ksto = Kstoch(q,sigmax,sigmaxi) if not Gaussian_process_noise else Kstoch(q,T.tile(q,(1,DIM)).reshape((N*DIM,DIM)),sigmaxi)
    if norm_sigma:
        Ksto = T.sqrt(2)*Ksto/T.sqrt(T.sum(T.sqr(Ksto),1))[:,np.newaxis]
    return Ksto
Ksto_sigmaf = function([qe],Ksto_sigma(qe))

# define sigma tensor (N,DIM,J)
def sigmalambdaf(q,lambdas):
    Ksto = Ksto_sigma(q)
    return (Ksto[:,:,np.newaxis]*lambdax(lambdas)[np.newaxis,:,:]).dimshuffle(0,2,1)
def sigmaf(q):
    return sigmalambdaf(q,lambdas)
def sigma_no_lambdaf(q):
    Ksto = Ksto_sigma(q)
    return (Ksto[:,:,np.newaxis]*T.tile(T.eye(DIM),(J//DIM,1))[np.newaxis,:,:]).dimshuffle(0,2,1)

# define pseudo-inverse sigma tensor (J,N,DIM)
def sigmaplusf(q):
    if basis != 'std':
        sigma = sigmaf(q)
        sigmaplus = theano.gradient.disconnected_grad(T.nlinalg.MatrixPinv()(sigma.reshape((N*DIM,J))).reshape((J,N,DIM))) # BEWARE OF DISCONNECTED GRADIENT. Gradient of MatrixPinv not available in Theano presently
    else:
        print("sigmaplusf: using standard basis")
        Ksto = Ksto_sigma(q)
        Kstoplus = theano.gradient.disconnected_grad(T.nlinalg.MatrixPinv()(Ksto)) # BEWARE OF DISCONNECTED GRADIENT. Gradient of MatrixPinv not available in Theano presently
        sigma = (Ksto[:,:,np.newaxis]*lambdax(lambdas)[np.newaxis,:,:]).dimshuffle(0,2,1)
        lambdaxplus = ((1/lambdax(lambdas).reshape((J//DIM,DIM,DIM)).sum(2))[:,:,np.newaxis]*T.eye(DIM)[np.newaxis,:,:]).reshape((J,DIM))
        sigmaplus = lambdaxplus[:,np.newaxis,:]*Kstoplus[:,:,np.newaxis]*DIM
        
    return (sigma,sigmaplus)
def sigmaplus_no_lambdaf(q):
    if basis != 'std':
        sigma = sigma_no_lambdaf(q)
        sigmaplus = theano.gradient.disconnected_grad(T.nlinalg.MatrixPinv()(sigma.reshape((N*DIM,J))).reshape((J,N,DIM)))
    else:
        Ksto = Ksto_sigma(q)
        Kstoplus = theano.gradient.disconnected_grad(T.nlinalg.MatrixPinv()(Ksto)) # BEWARE OF DISCONNECTED GRADIENT. Gradient of MatrixPinv not available in Theano presently
        lambdaxx = T.tile(T.eye(DIM),(J//DIM,1))
        sigma = (Ksto[:,:,np.newaxis]*lambdaxx[np.newaxis,:,:]).dimshuffle(0,2,1)
        lambdaxplus = lambdaxx
        sigmaplus = lambdaxplus[:,np.newaxis,:]*Kstoplus[:,:,np.newaxis]*DIM
    return (sigma,sigmaplus)
sigmaplus_no_lambdaff = function([qe],sigmaplus_no_lambdaf(qe))

# include p part of noise matrix
def sigmaplus_qpf(x):
    q = x[0]
    p = x[1]
    sigma = sigmaf(q)
    dsigmap = T.diagonal(T.jacobian((p[:,:,np.newaxis]*sigma).flatten(),q).reshape((N*DIM,J,N*DIM)),axis1=0,axis2=2).dimshuffle((1,0)).reshape((N*DIM,J))
    sigma_qp = T.concatenate((sigma.reshape((N*DIM,J)),dsigmap)).reshape((2,N,DIM,J))

    sigmaplus_qp = theano.gradient.disconnected_grad(T.nlinalg.MatrixPinv()(sigma_qp.reshape((2*N*DIM,J))).reshape((J,2,N,DIM)))

    return (sigma_qp,sigmaplus_qp)
sigmaplus_qpff = function([xe],sigmaplus_qpf(xe))

sigmaff = function([qe],sigmaf(qe))
sigmalambdaff = function([qe],sigmalambdaf(qe,lambdas))
dsigmaff = function([qe],T.jacobian(sigmaf(qe).flatten(),qe))
sigmaplusff = function([qe],sigmaplusf(qe))
sigma_no_lambdaff = function([qe],sigma_no_lambdaf(qe))

#define a grid for the sigma's positions
def grid(xlow,xhigh,nx,ylow,yhigh,ny):
    return np.dstack(np.meshgrid(np.linspace(xlow,xhigh,int(nx)),np.linspace(ylow,yhigh,int(ny)))).reshape(int(nx*ny),DIM).astype(theano.config.floatX)

#defines an ellipse for initial conditions
def ellipse(cent, Amp): 
    return  np.vstack(( Amp[0]*np.cos(np.linspace(0,2*np.pi*(1-1./N.eval()),N.eval()))+cent[0], Amp[1]*np.sin(np.linspace(0,2*np.pi*(1-1./N.eval()),N.eval()))+cent[1] )).T

#function for estimating the centre and the centred variance
def estimate_qq(data_q):
    data_mean= data_q.sum(0)/data_q.shape[0]
    data= data_q - data_mean
    
    return [data_mean,(data[:,:,:,np.newaxis,np.newaxis]*data[:,np.newaxis,np.newaxis,:,:]).sum(0)/data.shape[0]]

# change the timesteps
def set_nsteps(ln_steps):
    n_steps.set_value(ln_steps)
# set target
def set_qtarget(target):
    qtarget0.set_value(target.astype(theano.config.floatX))
# set MLE parameters
def setLambdas():
    lambdas.set_value(paramsToLambdas(params.eval()[-Js.eval():]))


# print setup and landmark configuration
def print_conf(x0):   
    print("Number of timesteps n_steps:", n_steps.eval())    
    print("Kernel width SIGMA:",SIGMA.eval())
    print("Noise kernel width sigmaxi:", sigmaxi.eval())
    print("Number of landmarks N:", N.eval())
    print("\n")
    print("Reduced parameters:", reduced_parameters, ", Gaussian process noise:", Gaussian_process_noise, ", normalized sigma rows:", norm_sigma)
    try:
        attraction.force.eval()
        print("Attraction force:", attraction_force.eval())
    except:
        pass
    print("Noise kernel:", sigma_kernel)
    print("Number of noise fields J:", J.eval())
    print("Number of noise parameters Js:", Js.eval())
    print("Noise parameters lambdas:", lambdasf())
    print("\n")
    print("initial positions q0:\n", x0[0].T)
    print("initial momentum p0:\n", x0[1].T)
    print("noise centers sigmax:\n", sigmax.eval().T)
    print("\n")
# plot noise and landmark configuration
from src.plotting import *
def plot_conf(x0):
    plot_sigmas_ellipses(coeff=1)
    plot_landmarks(x0)
    plt.axis([np.min(sigmax.eval()[:,0]),np.max(sigmax.eval()[:,0]),np.min(sigmax.eval()[:,1]),np.max(sigmax.eval()[:,1])])

# print MLE optimization setup
def print_optim_conf(options):
    print("Samples per obs:", options['samplesperobs'])
    print("Epochs: ", options['Epochs'])
    print("batch_size:", options['batch_size'])
    print("learning_rate:", options['learning_rate'])
    print("init_params:", options['init_params'])
    print("q0_fixed:", options['q0_fixed'], ", p0_fixed:", options['p0_fixed'], ", lambda_fixed:", options['lambda_fixed'])
    print("\n")

# print MLE parameters
def printParams():
    print(params.eval()[0:N.eval()*DIM].reshape((N.eval(),DIM)).T)
    print(params.eval()[N.eval()*DIM:2*N.eval()*DIM].reshape((N.eval(),DIM)).T)
    print(paramsToLambdas(params.eval().flatten()[-Js.eval():]))
def printLambdas():
    print(lambdas.eval())

