#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import * 
from src.landmark_ito import * 

############################################
#the shooting method with the ito correction#
#############################################

#compute loss functions
lossq = T.sum(T.sqr(cout_ito[-1,0]-qtarget0)) 

dlossq = T.grad(lossq,x) 
 
lossfq = function(inputs=[x],  
                            outputs=lossq,  
                            updates=updates_ito) 
 
dlossfq = function(inputs=[x],  
                            outputs=[lossq, dlossq],  
                            updates=updates_ito) 
 
# shooting function
from scipy.optimize import minimize 
def shoot_ito(q0,p0): 
     
    def fopts(x): 
        [y,gy] = dlossfq(np.stack([q0,x.reshape([N.eval(),DIM])]).astype(theano.config.floatX)) 
        return (y,gy[1].flatten()) 
    res = minimize(fopts, p0.flatten(), method='L-BFGS-B', jac=True, options={'disp': False, 'maxiter': maxiter}) 
    
    return(res.x,res.fun) 
