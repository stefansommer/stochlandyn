#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 10,6

from src.params import * 
from src.utils import * 

##############################
# various plotting functions #
##############################

#plot landmarks trajectories
def plot_landmarks_traj(x,lw=.1):
    if len(x.shape) == 2:
        x = x.reshape((1,1,)+x.shape)
    if len(x.shape) == 3:
        x = x.reshape((1,)+x.shape)    
    if len(x.shape) == 5:
        for i in range(x.shape[0]):
            plot_landmarks_traj(x[i])
        return

    for i in range(N.eval()):
        plt.plot(x[:,0,i,0],x[:,0,i,1],'k-',lw=lw)
        
#same but with initial and final conditions
def plot_landmarks(x,x0=None,lw=1.,line_style='g--',markersize=5,color='b',start_style='x-',end_style='o-'):
    if len(x.shape) == 2:
        x = x.reshape((1,1,)+x.shape)
    if len(x.shape) == 3:
        x = x.reshape((1,)+x.shape)
    if len(x.shape) == 5:
        for i in range(x.shape[0]):
            plot_landmarks(x[i],x0=x0,lw=lw,line_style=line_style,start_style=start_style,end_style=end_style,markersize=markersize,color=color)
        return
    if not x0 is None:
        x = np.concatenate((x0.reshape((1,)+x0.shape),x),axis=0)
    
    plt.plot(np.concatenate((x[0,0,:,0],[x[0,0,0,0],])),np.concatenate((x[0,0,:,1],[x[0,0,0,1],])),start_style,color=color,markersize=markersize)
    if x.shape[0] > 1:
        plt.plot(np.concatenate((x[-1,0,:,0],[x[-1,0,0,0],])),np.concatenate((x[-1,0,:,1],[x[-1,0,0,1],])),end_style,color=color,markersize=markersize)

    for i in range(x.shape[2]):
        plt.plot(x[:,0,i,0],x[:,0,i,1],line_style,lw=lw)

#plot the final variance from the moments
def plot_final(q,QQ,coeff=1.,c='m'):
    # plot sigma as ellipses 
    from matplotlib.patches import Ellipse
    from numpy import linalg as LA
    ax= plt.gca()
    for i in range(N.eval()):
        qq_eig,qq_vec = LA.eig(QQ[i,:,i,:])
        #print(QQ[i,:,i,:])
        vecx=np.array([q[i][0]-coeff*qq_eig[0]*qq_vec[0,0],q[i][0]+coeff*qq_eig[0]*qq_vec[0,0]])
        vecy=np.array([q[i][1]-coeff*qq_eig[0]*qq_vec[1,0],q[i][1]+coeff*qq_eig[0]*qq_vec[1,0]])
        plt.plot(vecx,vecy,lw=2,c=c)
        vecx=np.array([q[i][0]-coeff*qq_eig[1]*qq_vec[0,1],q[i][0]+coeff*qq_eig[1]*qq_vec[0,1]])
        vecy=np.array([q[i][1]-coeff*qq_eig[1]*qq_vec[1,1],q[i][1]+coeff*qq_eig[1]*qq_vec[1,1]])
        
        plt.plot(vecx,vecy,lw=2,c=c)

#plot the final variance from the moments
def plot_final_ellipses(q,QQ,coeff=1.,c='m',ls='-',lw=1):
    # plot sigma as ellipses 
    from matplotlib.patches import Ellipse
    from numpy import linalg as LA
    ax= plt.gca()
    for i in range(N.eval()):
        qq_eig,qq_vec = LA.eig(QQ[i,:,i,:])
        qq_eig = np.sqrt(qq_eig)
        theta = np.degrees(np.arctan(qq_vec[1,0]/qq_vec[0,0]))

        ell= Ellipse(xy=q[i] ,width=coeff*qq_eig[0],height= coeff*qq_eig[1],angle=theta,ls=ls,lw=lw)
        ax.add_artist(ell)
        ell.set_alpha(1.)
        ell.set_facecolor('None')
        ell.set_edgecolor(c)
        

#represent the noise fields 
def plot_sigmas(coeff=1.,c='k',lwidth=0.0001):
    # plot sigma as ellipses 
    from matplotlib.patches import Ellipse
    ax= plt.gca()
    for i in range(J.eval()):
        #ell= Ellipse(xy= sigmax.get_value()[i],width=coeff*lambdaxf()[i,0],height= lambdaxf()[i,0])
        ax.arrow(sigmax.get_value()[i][0], sigmax.get_value()[i][1],coeff*lambdaxf()[i,0], coeff*lambdaxf()[i,1] ,width= lwidth,head_width=0.02, head_length=0.01, fc=c, ec=c,alpha=1.)
        ax.arrow(sigmax.get_value()[i][0], sigmax.get_value()[i][1],-coeff*lambdaxf()[i,0], -coeff*lambdaxf()[i,1] ,width= lwidth,head_width=0.02, head_length=0.01, fc=c, ec=c,alpha=1.)
#     for i in range(0,J.eval(),2):
#         ell= Ellipse(xy= sigmax.get_value()[i],width=coeff*lambdaxf()[i,0],height= lambdaxf()[i+1,1])
#         ax.add_artist(ell)
#         ell.set_alpha(.5)
#         ell.set_facecolor(c)
#         ell.set_edgecolor(c)   


#plot density distribution of landmarks
def plot_distribution(xss):

    xTx=[]
    xTy=[]
    for i in range(0,len(xss)):
        for j in range(0,N.eval()):
            xTx.append(xss[i,j,0])
            xTy.append(xss[i,j,1])
    hist,histy,histx= np.histogram2d(xTy,xTx,bins=25)
    extent = [histx[0],histx[-1],histy[0],histy[-1]]

    
    #plt.contour(hist/np.max(hist),extent=extent,levels=[0.05,0.2,0.4,0.6],zorder=10)
    plt.imshow(hist/np.max(hist),extent=extent,interpolation='bicubic',origin='lower',cmap='Greys')#,levels=[0.05,0.2,0.4,0.6],zorder=10)
    #plt.colorbar()

#old functions

#represent the noise fields
def plot_sigmas_ellipses(coeff=1.):
    # plot sigma as ellipses 
    from matplotlib.patches import Ellipse
    ax= plt.gca()
    for i in range(J.eval()):
        ell= Ellipse(xy= sigmax.get_value()[i],width=coeff*lambdaxf()[i,0],height= coeff*lambdaxf()[i,1])
        ax.add_artist(ell)
        ell.set_alpha(0.7)
        ell.set_facecolor('g')


# plot noise amplitude grid
def plot_noise_amplitude(xy=False,no_lambda=None,border=0.):
    assert(not Gaussian_process_noise) # not implmented

    minxy = np.min(sigmax.eval(),0)
    maxxy = np.max(sigmax.eval(),0)
    grd= grid(-minxy[0]-border,minxy[1]+border,100,-maxxy[0]-border,maxxy[1]+border,100)

    NN = N.eval()
    N.set_value(1)
    
    if not no_lambda and norm_sigma:
        print("note: norm_sigma is True")

    vgrd = np.zeros((grd.shape[0],DIM))
    print()
    for i in range(grd.shape[0]):
        if not no_lambda:
            #vgrd[i] = np.sqrt((Ksto_sigmaf(grd[i].reshape((1,DIM)))**2).reshape((J.eval()/DIM,DIM)).sum(0))
            vgrd[i] = np.sqrt((sigmaff(grd[i].reshape((1,DIM)))**2).sum((0,2)))
        else:
            #vgrd[i] = np.sqrt((Kstochf(grd[i].reshape((1,DIM)))**2).reshape((J.eval()/DIM,DIM)).sum(0))
            vgrd[i] = np.sqrt((sigma_no_lambdaff(grd[i].reshape((1,DIM)))**2).sum((0,2)))
        
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib import cm
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    if xy: # plot different colors for x and y amplitude
        ax.plot_surface(grd.reshape((100,100,DIM))[:,:,0],
                            grd.reshape((100,100,DIM))[:,:,1],
                            vgrd[:,0].reshape((100,100)),
                            color='g')
        ax.plot_surface(grd.reshape((100,100,DIM))[:,:,0],
                            grd.reshape((100,100,DIM))[:,:,1],
                            vgrd[:,1].reshape((100,100)),
                            color='b')
        plt.xlabel('green: x variance')
        plt.ylabel('blue: y variance')
    else: # plot total amplitude divided by DIM
        ax.plot_surface(grd.reshape((100,100,DIM))[:,:,0],
                            grd.reshape((100,100,DIM))[:,:,1],
                            vgrd.sum(1).reshape((100,100))/DIM,
                            color='b')
        plt.xlabel('per axis total noise variance')
    zlim = ax.get_zlim3d(); zlim[0] = 0.; ax.set_zlim3d(zlim)

    N.set_value(NN)
