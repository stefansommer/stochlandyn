#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

from src.setup import *

###############################################################
################### deterministic ODE integration #############
###############################################################

def ode_f(x):
    
    dqt = dq(x[0],x[1])
    dpt = dp(x[0],x[1])
        
    return T.stack((dqt,dpt))

def euler(x,dt):    
    return x+dt*ode_f(x)

# create loop symbolic loop
(cout, updates) = theano.scan(fn=euler,
                                outputs_info=[x],
                                non_sequences=[dt],
                                n_steps=n_steps)

# compile it
if theano.config.device != 'cuda':
    simf = function(inputs=[x], 
                            outputs=cout, 
                            updates=updates)
else:
    sim_gpuf = function(inputs=[x], 
                    outputs=cout.transfer(None), 
                    updates=updates)
    simf = lambda  x: np.asarray(sim_gpuf(x))

# ... and loss functions
loss = 1./N*T.sum(T.sqr(cout[-1,0]-qtarget0))

dloss = T.grad(loss,x)

if theano.config.device != 'cuda':
    lossf = function(inputs=[x], 
                            outputs=loss, 
                            updates=updates)
    dlossf = function(inputs=[x], 
                            outputs=[loss, dloss], 
                            updates=updates)
else:
    sim_gpuf = function(inputs=[x], 
                    outputs=cout.transfer(None), 
                    updates=updates)
    simf = lambda  x: np.asarray(sim_gpuf(x))
    loss_gpuf = function(inputs=[x], 
                            outputs=loss.transfer(None), 
                            updates=updates)
    lossf = lambda  x: np.asarray(loss_gpuf(x))
    dloss_gpuf = function(inputs=[x], 
                            outputs=[loss.transfer(None), dloss.transfer(None)], 
                            updates=updates)
    def dlossf(x):
        res = dloss_gpuf(x)
        return (np.asarray(res[0]),np.asarray(res[1]))


# shooting
from scipy.optimize import minimize,fmin_bfgs,fmin_cg

def shoot(q0,p0):
    def fopts(x):
        [y,gy] = dlossf(np.stack([q0,x.reshape([N.eval(),DIM])]).astype(theano.config.floatX),)
        return (y,gy[1].flatten())
    
    res = minimize(fopts, p0.flatten(), method='L-BFGS-B', jac=True, options={'disp': False, 'maxiter': maxiter})
    
    return(res.x,res.fun)
