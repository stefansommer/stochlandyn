#
# This file is part of stochlandyn.
#
# Copyright (C) 2017, Alexis Arnaudon (alexis.arnaudon@imperial.ac.uk), Stefan Sommer (sommer@di.ku.dk)
# https://bitbucket.org/stefansommer/stochlandyn
#
# stochlandyn is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochlandyn is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with stochlandyn.  If not, see <http://www.gnu.org/licenses/>.
#

import theano
import numpy as np
import theano.tensor as T

import src.multiprocess_utils as mpu

from src.landmark_det import *
from src.landmark_guided import *

##########################################################################
# MLE and EM/SAEM functions                                              #
##########################################################################

def sampleprop(target,noise,lparams):
    set_qtarget(target)
    params.set_value(lparams)
    setLambdas()
    (loglike,dloglike,logalpha,xs) = rsimf_guided_loglike_params(noise)
    qs = xs[:,0,:,:]
    return (loglike,dloglike,logalpha,qs)

def pbatch_mp(lparams,options,pars):
    (l,obs,avgalpha,seed,npseed) = pars
    
    srng.seed(seed)
    np.random.seed(npseed)
    params.set_value(lparams.astype(theano.config.floatX))
    setLambdas()
    #qss = np.zeros([options['samplesperobs'],n_steps.eval(),N.eval()*DIM])
    loglikes = np.zeros([options['samplesperobs']])   
    dloglikes = np.zeros([options['samplesperobs'],2*N.eval()*DIM+Js.eval()])
    logalphas = np.zeros([options['samplesperobs']])
    i = 0; j=0
    while i < options['samplesperobs'] and j < options['samplesperobs']*10:
        j = j+1
        ldWs = dWsf()
        (loglike,dloglike,logalpha,_) = sampleprop(obs.reshape([N.eval(),DIM]),ldWs,params.eval())
        loglikes[i] = loglike
        dloglikes[i] = dloglike
        if np.linalg.norm(dloglike,2)*options['learning_rate'] > 10000.:
            print("skip sample, scaled gradient norm:", np.linalg.norm(dloglike,2)*options['learning_rate'])
            continue
        #qss[i] = qs.reshape((n_steps.eval(),N.eval()*DIM))
        logalphas[i] = logalpha
        i = i+1
    if j >= options['samplesperobs']*5:
        printParams()
        assert(False) # max iteration limit
    alphas = np.exp(logalphas)
    avgalpha = .1*np.mean(alphas)+.9*avgalpha # incorporate previous estimate to reduce needed sampling
    alphas = alphas/avgalpha
 
    return (loglikes,dloglikes,alphas,avgalpha)#,qss)

def EM(obs,options):
    # init
    K = obs.shape[0] # number observations
    batch_learning_rate = options['learning_rate']/(K//options['batch_size'])
    params.set_value(options['init_params'].astype(theano.config.floatX))
    setLambdas()
    printParams()
    printLambdas()
    try:
        attraction_force.set_value(options['attraction_force'])
    except:
        pass
    
    n_batches = len(range(0,K,options['batch_size']))
    
    paramss = np.zeros((options['Epochs'],)+params.get_value().shape)
    avglosses = np.zeros(options['Epochs'])
    
    print("starting stochastic EM...")
    meanOptEpocs = 0 # only optimize for mean this many epocs in beginning of optimization
    #paramsmin = params.get_value() # controlling optimization and learning rate
    #mincounter = 0
    #avglossmin = 1e6
    avgalphas = np.ones(K)
    try:
        mpu.openPool()
        for epoch in range(options['Epochs']):
            print('Epoch %d (learning rate %e)....' % (epoch,batch_learning_rate))
            avgloss = 0.
            for k in range(0,K,options['batch_size']): 
                #print("epoch %d, batch %d" % (epoch,k//batch_size))
                batch = obs[k:k+options['batch_size']]
                this_batch_size = batch.shape[0]
                batchavgalphas = avgalphas[k:k+options['batch_size']]
    
                sol = mpu.pool.imap(partial(pbatch_mp,params.eval(),options),\
                        mpu.inputArgs(range(this_batch_size),batch,batchavgalphas,\
                                  np.random.randint(1000,size=this_batch_size),np.random.randint(1000,size=this_batch_size),\
                                 ))        
                res = list(sol)
                batchloglikes = mpu.getRes(res,0).reshape([this_batch_size,options['samplesperobs']])
                batchdloglikes = mpu.getRes(res,1).reshape([this_batch_size,options['samplesperobs'],2*N.eval()*DIM+Js.eval()])
                batchalphas = mpu.getRes(res,2).reshape([this_batch_size,options['samplesperobs']])
                batchavgalphas = mpu.getRes(res,3).reshape([this_batch_size])
                #batchqss = mpu.getRes(res,4).reshape([this_batch_size,options['samplesperobs'],n_steps.eval(),N.eval()*DIM])
            
                avgalphas[k:k+options['batch_size']] = batchavgalphas
    
                # combine samples
                batchloss = -np.mean(batchalphas*batchloglikes)
                dbatchloss = -np.mean(batchalphas[:,:,np.newaxis]*batchdloglikes,axis=(0,1))
                # fix or rescale different parts of gradients
                dbatchloss[0:N.eval()*DIM] = np.zeros(N.eval()*DIM) if options['q0_fixed'] else options['learning_rate_q0']*dbatchloss[0:N.eval()*DIM]
                dbatchloss[N.eval()*DIM:2*N.eval()*DIM] = np.zeros(N.eval()*DIM) if options['p0_fixed'] else options['learning_rate_p0']*dbatchloss[N.eval()*DIM:2*N.eval()*DIM]
                dbatchloss[-Js.eval():] = np.zeros(Js.eval()) if options['lambda_fixed'] else options['learning_rate_lambdas']*dbatchloss[-Js.eval():]
                # take step
                lparams = params.get_value()
                lparams = lparams-batch_learning_rate*dbatchloss
                params.set_value(lparams)
    #         print('batch loss %e' % batchloss) 
    #         print(dbatchloss)
    #         printParams()
                avgloss += batchloss/n_batches
    
    #     # output state visualize
    #     setLambdas()
    #     plot_mle_iter()
    #     plt.title(r"$likelihood={0:e}$".format(float(avgloss)))
    #     plt.savefig("shooting/images/im_{0:s}.png".format(str(epoch).zfill(3)))
    #     display.display(plt.gcf())
    #     display.clear_output()
        
            print("epoch avg loss %f" % avgloss)
            printParams()
    #     print(avgalphas)
        # save
            avglosses[epoch] = avgloss
            paramss[epoch] = params.get_value()
    except:
        mpu.closePool()
        printLambdas()
        printParams()
        raise
    else:
        mpu.closePool()
    
    return (avgloss,params.eval(),paramss,avglosses)


# plotting
from IPython import display
from src.landmark_ito import *

def plot_mle_iter(x0,q0,obs,Knew,use_obs=False,coeff=1.):
    K = obs.shape[0]
    xsu = simf(x0)
    xsmean = simf_ito(x0)
    xsres = simf_ito(params.eval()[0:2*N.eval()*DIM].reshape((2,N.eval(),DIM)))

    obsnew = np.array(np.zeros((K,)+q0.shape)).astype(theano.config.floatX)
    if use_obs:
        obsnew = obs
    else:
        xss = np.array(np.zeros((K,n_steps.eval(),2,N.eval(),DIM))).astype(theano.config.floatX)
        for i in range(K):
            xss[i] = rsimf(params.eval()[0:2*N.eval()*DIM].reshape((2,N.eval(),DIM)),dWsf())
            obsnew[i] = xss[i,-1,0]

    # plot
    #%matplotlib inline
    #rcParams['figure.figsize'] = 15, 10
    plot_distribution(obsnew)    
    if False: # not use_obs:
        plot_landmarks(xss[0:Knew],x0=params.eval()[0:2*N.eval()*DIM].reshape((2,N.eval(),DIM)),lw=.5,line_style='g-')
    else:
        q0new = params.eval()[0:N.eval()*DIM].reshape((N.eval(),DIM))
        plt.plot(np.concatenate((q0new[:,0],[q0new[0,0],])),np.concatenate((q0new[:,1],[q0new[0,1],])),'bx-')
        colormap = plt.get_cmap('winter')
        colors=[colormap(k) for k in np.linspace(0, 1, Knew)]
        for i in range(Knew):
            plt.plot(np.concatenate((obsnew[i,:,0],obsnew[i,0:0,0])).T,np.concatenate((obsnew[i,:,1],obsnew[i,0:0,1])).T,'.-',color=colors[i])

    if coeff > .0:
        plot_sigmas(coeff=coeff)
#     plt.axis('off')
#     plt.axis('equal')
    
    if coeff > 0.:
        target2= estimate_qq(obsnew)
        plot_final_ellipses(target2[0],target2[1],coeff=coeff,c='r') #for sampling

